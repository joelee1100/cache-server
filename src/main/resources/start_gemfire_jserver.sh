#!/bin/bash
TARGETDIR=/Users/abc/GemfireServer/target
TARGET=GemfireServer-0.0.1-SNAPSHOT.jar

echo Starting gemfire server $1
java -jar ${TARGETDIR}/${TARGET} --spring.data.gemfire.name=$1

