package com.lee.joseph.GemfireServer.cache.loader;

import com.lee.joseph.GemfireServer.repository.LockboxRepository;
import com.lee.joseph.common.cache.entity.Lockbox;
import org.apache.geode.cache.CacheLoader;
import org.apache.geode.cache.CacheLoaderException;
import org.apache.geode.cache.LoaderHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LockboxCacheLoader implements CacheLoader<String, Lockbox> {

    private LockboxRepository lockboxRepository;

    @Autowired
    public LockboxCacheLoader(LockboxRepository lockboxRepository) {
        this.lockboxRepository = lockboxRepository;
    }

    @Override
    public Lockbox load(LoaderHelper<String, Lockbox> loaderHelper) throws CacheLoaderException {
        return new Lockbox(loaderHelper.getKey(), "Lock Box 1");
    }

    @Override
    public void close() {

    }

}
