package com.lee.joseph.GemfireServer.cache.loader;

import com.lee.joseph.common.cache.entity.Person;
import org.apache.geode.cache.CacheLoader;
import org.apache.geode.cache.CacheLoaderException;
import org.apache.geode.cache.LoaderHelper;
import org.springframework.stereotype.Component;

@Component
public class PeopleCacheLoader implements CacheLoader<Long, Person> {
    @Override
    public Person load(LoaderHelper<Long, Person> loaderHelper) throws CacheLoaderException {
        System.out.println("Key=" + loaderHelper.getKey());
        return new Person(999L, "John Doe", 20);
    }

    @Override
    public void close() {

    }
}
