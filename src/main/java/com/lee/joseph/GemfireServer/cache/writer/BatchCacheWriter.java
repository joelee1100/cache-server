package com.lee.joseph.GemfireServer.cache.writer;

import com.lee.joseph.common.cache.entity.Batch;
import org.apache.geode.cache.CacheWriter;
import org.apache.geode.cache.CacheWriterException;
import org.apache.geode.cache.EntryEvent;
import org.apache.geode.cache.RegionEvent;
import org.springframework.stereotype.Component;

@Component
public class BatchCacheWriter implements CacheWriter<String, Batch> {

    @Override
    public void beforeUpdate(EntryEvent<String, Batch> entryEvent) throws CacheWriterException {

    }

    @Override
    public void beforeCreate(EntryEvent<String, Batch> entryEvent) throws CacheWriterException {

    }

    @Override
    public void beforeDestroy(EntryEvent<String, Batch> entryEvent) throws CacheWriterException {

    }

    @Override
    public void beforeRegionDestroy(RegionEvent<String, Batch> regionEvent) throws CacheWriterException {

    }

    @Override
    public void beforeRegionClear(RegionEvent<String, Batch> regionEvent) throws CacheWriterException {

    }

    @Override
    public void close() {

    }
}
