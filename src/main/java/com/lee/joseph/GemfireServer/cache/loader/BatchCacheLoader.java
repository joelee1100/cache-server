package com.lee.joseph.GemfireServer.cache.loader;

import com.lee.joseph.GemfireServer.repository.BatchRepository;
import com.lee.joseph.common.cache.entity.Batch;
import org.apache.geode.cache.CacheLoader;
import org.apache.geode.cache.CacheLoaderException;
import org.apache.geode.cache.LoaderHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BatchCacheLoader implements CacheLoader<String, Batch> {

    private BatchRepository batchRepository;

    @Autowired
    public BatchCacheLoader(BatchRepository batchRepository) {
        this.batchRepository = batchRepository;
    }

    @Override
    public Batch load(LoaderHelper<String, Batch> loaderHelper) throws CacheLoaderException {
//        return this.batchRepository.findById(loaderHelper.getKey());
        return new Batch(loaderHelper.getKey(), "999", "999", "999", "999");
    }

    @Override
    public void close() {

    }
}
