package com.lee.joseph.GemfireServer;

import com.lee.joseph.GemfireServer.config.CacheRegionConfiguration;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(CacheRegionConfiguration.class)
public class GemfireServerApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(GemfireServerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

	}
}
