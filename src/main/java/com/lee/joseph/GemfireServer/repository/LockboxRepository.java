package com.lee.joseph.GemfireServer.repository;

import com.lee.joseph.GemfireServer.entity.LockboxEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface LockboxRepository extends CrudRepository<LockboxEntity, String> {
}
