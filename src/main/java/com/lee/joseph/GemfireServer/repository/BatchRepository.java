package com.lee.joseph.GemfireServer.repository;

import com.lee.joseph.GemfireServer.entity.BatchEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface BatchRepository extends CrudRepository<BatchEntity, String> {

}
