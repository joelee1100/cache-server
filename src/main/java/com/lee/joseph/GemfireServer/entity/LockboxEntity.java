package com.lee.joseph.GemfireServer.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Lockbox")
public class LockboxEntity implements Serializable {

    private static final long serialVersionUID = 41L;

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator( name = "UUID", strategy = "org.hibernate.id.UUIDGenerator" )
    @Column(name = "id", updatable = false, nullable = false)
    private String id;

    @Column(name = "Lockbox")
    private String lockbox;

    public String getId() {
        return id;
    }

    public String getLockbox() {
        return lockbox;
    }

    public void setLockbox(String lockbox) {
        this.lockbox = lockbox;
    }
}
