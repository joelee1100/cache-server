package com.lee.joseph.GemfireServer.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "Batch")
public class BatchEntity implements Serializable {

    private static final long serialVersionUID = 42L;

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator( name = "UUID", strategy = "org.hibernate.id.UUIDGenerator" )
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "LockboxId")
    private String lockboxId;

    @Column(name = "ReferenceNumber")
    private String referenceNumber;

    @Column(name = "BatchStateId")
    private String batchStateId;

    @Column(name = "ScanId")
    private String scanId;

    public UUID getId() {
        return id;
    }

    public String getLockboxId() {
        return lockboxId;
    }

    public void setLockboxId(String lockboxId) {
        this.lockboxId = lockboxId;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getBatchStateId() {
        return batchStateId;
    }

    public void setBatchStateId(String batchStateId) {
        this.batchStateId = batchStateId;
    }

    public String getScanId() {
        return scanId;
    }

    public void setScanId(String scanId) {
        this.scanId = scanId;
    }
}
