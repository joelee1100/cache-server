package com.lee.joseph.GemfireServer.config;


import com.lee.joseph.common.cache.entity.Person;
import org.apache.geode.cache.Cache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.gemfire.ReplicatedRegionFactoryBean;
import org.springframework.data.gemfire.config.annotation.CacheServerApplication;

@Configuration
@CacheServerApplication(locators = "localhost[10334]", port = 0)
public class CacheRegionConfiguration {

    @Bean
    public ReplicatedRegionFactoryBean peopleRegion (Cache gemfireCache) {

        ReplicatedRegionFactoryBean<Long, Person> people = new ReplicatedRegionFactoryBean<>();

        people.setCache(gemfireCache);
        people.setClose(false);
        people.setPersistent(false);
        people.setKeyConstraint(Long.class);
        people.setValueConstraint(Person.class);
        people.setName("people");

        return people;
    }

}
