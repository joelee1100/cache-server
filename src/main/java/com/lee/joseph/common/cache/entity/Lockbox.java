package com.lee.joseph.common.cache.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.gemfire.mapping.annotation.Region;

import java.io.Serializable;

@Region("Lockbox")
public class Lockbox implements Serializable {
    @Id
    private final String id;
    private final String lockbox;

    @PersistenceConstructor
    public Lockbox(String id, String lockbox) {
        this.id = id;
        this.lockbox = lockbox;
    }

    @Override
    public String toString() {
        return "Lockbox{" +
                "id='" + id + '\'' +
                ", lockbox='" + lockbox + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public String getLockbox() {
        return lockbox;
    }
}
