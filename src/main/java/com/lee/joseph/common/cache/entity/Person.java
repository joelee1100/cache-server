package com.lee.joseph.common.cache.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.gemfire.mapping.annotation.Region;

import java.io.Serializable;

@Region("people")
public class Person implements Serializable {
    @Id
    private final Long id;
    private final String name;
    private final int age;

    @PersistenceConstructor
    public Person(Long id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return String.format("%s is %d years old (id:%d)", getName(), getAge(), getId());
    }

    public Long getId() { return id; }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

}
