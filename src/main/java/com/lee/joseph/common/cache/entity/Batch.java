package com.lee.joseph.common.cache.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.gemfire.mapping.annotation.Region;

import java.io.Serializable;

@Region("Batch")
public class Batch implements Serializable {

    @Id
    private final String id;
    private final String lockboxId;
    private final String referenceNumber;
    private final String batchStateId;
    private final String scanId;

    @PersistenceConstructor
    public Batch(String id, String lockboxId, String referenceNumber, String batchStateId, String scanId) {
        this.id = id;
        this.lockboxId = lockboxId;
        this.referenceNumber = referenceNumber;
        this.batchStateId = batchStateId;
        this.scanId = scanId;
    }

    @Override
    public String toString() {
        return "Batch{" +
                "id='" + id + '\'' +
                ", lockboxId='" + lockboxId + '\'' +
                ", referenceNumber='" + referenceNumber + '\'' +
                ", batchStateId='" + batchStateId + '\'' +
                ", scanId='" + scanId + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public String getLockboxId() {
        return lockboxId;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public String getBatchStateId() {
        return batchStateId;
    }

    public String getScanId() {
        return scanId;
    }
}
